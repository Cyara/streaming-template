# Streaming Template

Template realizado con HTML5, CSS3 y JS, un sitio de streaming tipo Netflix o Disney+.

### Interfaz del proyecto

![](https://gitlab.com/Cyara/streaming-template/-/raw/master/img/portal.jpg)

> Imagen de referencia

## Información

| Nombre  |  Email |
| ------------ | ------------ |
|  Cristhian Yara |  cmyarap@gmail.com |